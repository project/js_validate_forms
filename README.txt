-----------------------
Description
-----------------------
The JavaScript Validate Forms project was built to prevent spam without getting 
in the way of user's interactions with forms. It, like many other spam 
prevention modules adds a field to your forms. However, unlike some other 
modules like Spamicide and Gotcha, it does not require the bot to fill in the 
bogus form item. JS Validate Forms uses JavaScript to fill in the form item with 
the GMT in seconds as passed in from the server (via Drupal.settings). The 
server then checks the submission to make sure that the field is filled in and 
that time has elapsed since the user accessed the page.

Bots normally do not run in a real browser. Thus they may lack JavaScript. Since 
JS is required to fill in the "hidden" form item, it should be helpful it 
controlling spam. If the bot fills in the validation field item, it will need 
incredible amounts of luck to insert a value that is accepted in the server side 
validation. Essentially, the bot will have to input a GMT timestamp, in seconds, 
that falls between the access time and a 24 hour window.


-----------------------
Usage
-----------------------

    * Simply download and enable the module in the usual way.
    * Then give the site admin role (if you have one - not all folks set up a 
      site admin role) the ability to 'configure js validate forms.'
    * Then navigate to the settings page at 
      http://example.com/?=admin/settings/js_validate_forms. 
      From here, simply supply the form ID's (or id regex patterns) you want to 
      protect in the text area.
    * Click the use checkbox when you are ready to enable the module on the 
      forms.
      
** Note: Will not validate your forms if "Use" checkbox remains unchecked. **


-----------------------
Rec. form id's/patterns
-----------------------
    
    * comment_form
    * contact_mail_page
    * /webform_client_form_\d/
    * /user_(login|register|pass)/
    
    
-----------------------
Concerns
-----------------------

If you use AJAX form submission on the forms you want to protect, this module 
will not work right. We would greatly appreciate any aid in getting the module 
to properly validate in an AJAX environment.
    


