
Drupal.behaviors.jsValidateFormsInitialize = function(context) {
	// Add a GMT time stamp to the verification field - pulled from Drupal Settings
	var $formValidateField = $('.user-validate');
	if ($formValidateField.length > 0) {
		$formValidateField.each(function(i) {
			$(this).val(Drupal.settings.user_validate);			
		});
	}
	// Add class to validation msg to get it out of sight.
	$('form .validate-message').addClass('validate-message-jsenabled');
}
